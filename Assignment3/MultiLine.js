
//margin setup
var margin = {top: 20, right: 80, bottom: 30, left: 50},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

var parseDate = d3.time.format("%Y").parse;

//creating scales
var x = d3.time.scale()
    .range([0, width-10]);

var y = d3.scale.linear()
    .range([height, 0]);

var color = d3.scale.category10();

//rendering axis
var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

var line = d3.svg.line()
    .interpolate("basis")
    .x(function(d) { return x(d.date); })
    .y(function(d) { return y(d.energy); });



//define svg
var svg = d3.select("body").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


//importing data
d3.csv("EPC_2000_2010_new.csv", function(error, data) {
  color.domain(d3.keys(data[0]).filter(function(key) { return key !== "year"; }));

  data.forEach(function(d) {
    d.year = parseDate(d.year);
  });

//variables
  var countries = color.domain().map(function(name) {
    return {
      name: name,
      values: data.map(function(d) {
        return {date: d.year, energy: +d[name]};
      })
    };
  });

  x.domain(d3.extent(data, function(d) { return d.year; }));

  y.domain([
    d3.min(countries, function(c) { return d3.min(c.values, function(v) { return v.energy; }); }),
    d3.max(countries, function(c) { return d3.max(c.values, function(v) { return v.energy; }); })
  ]);


    
    // Add the title
    svg.append("text")
        .attr("x", (width / 2))     
        .attr("y", 0 - (margin.top / 2 - 5))
        .attr("text-anchor", "middle")
        .style("font-size", "16px")
        .style("text-decoration", "underline")
        .text("Energy consumption per Capita");
    

//draw axes    
  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
      .append("text")
      .attr("transform", "rotate(-90)")
      .attr("x", (-width / 5)) 
      .attr("y", -40)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .style("text-anchor", "left")
      .text(" Energy Consumption per Capita (Million BTU's)");
    
    //functions for grid lines    
function make_x_axis(){
    return d3.svg.axis()
    .scale(x)
    .orient("bottom")
    .ticks(10)
    }
    
function make_y_axis(){
    return d3.svg.axis()
    .scale(y)
    .orient("left")
    .ticks(10)
    }

    svg.append("g")
        .attr("class", "grid")
        .attr("transform", "translate(0," + height + ")")
        .call(make_x_axis().tickSize(-height, 0, 0).tickFormat(""))
    svg.append("g")
        .attr("class", "grid")
        .call(make_y_axis().tickSize(-width, 0, 0).tickFormat(""))
    
    
    svg.append("text")
        .attr("transform", "translate(" + (width / 2) + " ," + (height + margin.bottom) + ")")
        .style("text-anchor", "middle")
        .text("Year");

//draw multi-lines
  var country = svg.selectAll(".country")
      .data(countries)
      .enter().append("g")
      .attr("class", "country");


  country.append("path")
      .attr("class", "line")
      .attr("d", function(d) { return line(d.values); })
      .attr('fill', 'none')
      .style("stroke", function(d) { return color(d.name); });

  country.append("text")
      .datum(function(d) { return {name: d.name, value: d.values[d.values.length - 1]}; })
      .attr("transform", function(d) { return "translate(" + x(d.value.date) + "," + y(d.value.energy) + ")"; })
      .attr("x", 3)
      .attr("dy", ".35em")
      .text(function(d) { return d.name; });
    
        
    
  /* Add 'curtain' rectangle to hide entire graph */
  var curtain = svg.append('rect')
    .attr('x', -1 * width)
    .attr('y', -1 * height)
    .attr('height', height)
    .attr('width', width)
    .attr('class', 'curtain')
    .attr('transform', 'rotate(180)')
    .style('fill', '#ffffff')

  /* Optionally add a guideline */
  var guideline = svg.append('line')
    .attr('stroke', '#333')
    .attr('stroke-width', 0)
    .attr('class', 'guide')
    .attr('x1', 1)
    .attr('y1', 1)
    .attr('x2', 1)
    .attr('y2', height)
    
 /* Create a shared transition for anything we're animating */
  var t = svg.transition()
    .delay(750)
    .duration(6000)
    .ease('linear')
    .each('end', function() {
      d3.select('line.guide')
        .transition()
        .style('opacity', 0)
        .remove()
    });

  t.select('rect.curtain')
    .attr('width', 0);
  t.select('line.guide')
    .attr('transform', 'translate(' + width + ', 0)')

  d3.select("#show_guideline").on("change", function(e) {
    guideline.attr('stroke-width', this.checked ? 1 : 0);
    curtain.attr("opacity", this.checked ? 0.75 : 1);
  })
    
    
});
